﻿using System;
using System.Collections.Generic;

namespace GTween
{
	//Tween队列
	public class TweenSequence
	{
		public string id { get; private set; }
		public bool loop { get; private set; }
		public bool complete { get; private set; }
		public float passTime { get; private set; }
		public int currentTweenIndex { get; private set; }
		public Tween currentTween => currentTweenIndex < tweens.Count ? tweens[currentTweenIndex] : null;
		private List<Tween> tweens = new List<Tween>();
		private Action<TweenSequence> completeFunc;

		public TweenSequence(string id, bool loop)
		{
			this.id = id;
			this.loop = loop;
			passTime = 0;
			currentTweenIndex = 0;
			complete = false;
		}

		public void Reset()
		{
			foreach (var tween in tweens)
			{
				tween.Reset();
			}
			passTime = 0;
			currentTweenIndex = 0;
			complete = false;
		}

		public void UpdateTweenSequence(float deltaTime, bool autoCallCompleteFunc = false)
		{
			if (complete) return;
			passTime += deltaTime;
			if (currentTween == null)
			{
				complete = true;
				if (autoCallCompleteFunc)
				{
					completeFunc?.Invoke(this);
				}
				return;
			}
			currentTween.UpdateTween(deltaTime, true);
			if (currentTween.complete)
			{
				currentTweenIndex++;
			}
		}

		public void AddTween(Tween tween)
		{
			tweens.Add(tween);
		}

		public void CallCompleteFunc()
		{
			completeFunc?.Invoke(this);
		}
	}
}
