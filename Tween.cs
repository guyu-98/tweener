﻿using System;
using System.Collections.Generic;

namespace GTween
{
	public class Tween
	{
		public string id { get; private set; } 
		public float passTime { get; private set; }
		public float duration { get; private set; }
		public bool loop { get; private set; }
		public bool complete { get; private set; }
		private Action<Tween> updateFunc;
		private Action<Tween> completeFunc;
		private List<TweenEvent> events = new List<TweenEvent>();

		public Tween(string id, float duration, Action<Tween> updateFunc = null, Action<Tween> completeFunc = null, bool loop = false)
		{
			this.id = id;
			passTime = 0;
			this.duration = duration;
			this.updateFunc = updateFunc;
			this.completeFunc = completeFunc;
			this.loop = loop;
			complete = false;
		}

		public void Reset()
		{
			passTime = 0;
			foreach (var theEvent in events)
			{
				theEvent.Reset();
			}
			complete = false;
		}

		public void UpdateTween(float deltaTime, bool autoCallCompleteFunc = false)
		{
			if (complete) return;
			passTime += deltaTime;

			foreach (var theEvent in events)
			{
				theEvent.CheckEvent(this, passTime);
			}
			
			updateFunc?.Invoke(this);
			if (passTime >= duration)
			{
				if (autoCallCompleteFunc)
				{
					CallCompleteFunc();
				}
				complete = true;
			}
		}

		public void CallCompleteFunc()
		{
			completeFunc?.Invoke(this);
		}

		public void AddEvent(TweenEvent tweenEvent)
		{
			events.Add(tweenEvent);
		}
	}
}
