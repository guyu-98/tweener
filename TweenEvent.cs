﻿using System;

namespace GTween
{
	public class TweenEvent
	{
		public float triggerTime { get; private set; }
		public bool triggerred { get; private set; }
		private Action<Tween> callBack;

		public TweenEvent(float triggerTime, Action<Tween> callBack)
		{
			this.triggerTime = triggerTime;
			this.callBack = callBack;
			triggerred = false;
		}

		public void Reset()
		{
			triggerred = false;
		}

		public void CheckEvent(Tween tween, float currentTime)
		{
			if (triggerred) return;
			if (currentTime >= triggerTime)
			{
				triggerred = true;
				callBack?.Invoke(tween);
			}
		}
	}
}
