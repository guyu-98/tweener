using System.Collections.Generic;

namespace GTween
{
	public class Tweener
	{
		private Dictionary<string, Tween> tweens = new Dictionary<string, Tween>();
		//匿名tween，无id，无法被相同id的tween覆盖
		private List<Tween> anonymousTweens = new List<Tween>();
		private Dictionary<string, TweenSequence> tweenSequences = new Dictionary<string, TweenSequence>();
		private List<TweenSequence> anonymousTweenSequences = new List<TweenSequence>();
		
		public void Update(float deltaTime)
		{
			UpdateTweens(deltaTime);
			UpdateTweenSequences(deltaTime);
		}

		public void AddTween(Tween tween)
		{
			if (tween.id != null)
			{
				tweens[tween.id] = tween;
			}
			else
			{
				anonymousTweens.Add(tween);
			}
		}

		public void AddTweenSequence(TweenSequence tweenSequence)
		{
			if (tweenSequence.id != null)
			{
				tweenSequences[tweenSequence.id] = tweenSequence;
			}
			else
			{
				anonymousTweenSequences.Add(tweenSequence);
			}
		}

		public Tween RemoveTween(Tween tween)
		{
			if (tween.id != null)
			{
				tweens.Remove(tween.id);
				return tween;
			}
			anonymousTweens.Remove(tween);
			return tween;
		}

		public Tween RemoveTween(string id)
		{
			var tween = tweens[id];
			tweens.Remove(id);
			return tween;
		}

		private void UpdateTweens(float deltaTime)
		{
			List<string> completeTweenKeys = new List<string>();
			List<Tween> completeTweens = new List<Tween>();
			//更新tween
			foreach (var kvp in tweens)
			{
				kvp.Value.UpdateTween(deltaTime);
				if (kvp.Value.complete)
				{
					completeTweenKeys.Add(kvp.Key);
				}
			}
			foreach (var tween in anonymousTweens)
			{
				tween.UpdateTween(deltaTime);
				if (tween.complete)
				{
					completeTweens.Add(tween);
				}
			}

			//处理已完成tween
			foreach (var key in completeTweenKeys)
			{
				var tween = tweens[key];
				if (tween.loop)
				{
					tween.CallCompleteFunc();
					tween.Reset();
				}
				else
				{
					tweens.Remove(key);
					tween.CallCompleteFunc();
				}
			}
			foreach (var tween in completeTweens)
			{
				if (tween.loop)
				{
					tween.CallCompleteFunc();
					tween.Reset();
				}
				else
				{
					anonymousTweens.Remove(tween);
					tween.CallCompleteFunc();
				}
			}
		}
		
		private void UpdateTweenSequences(float deltaTime)
		{
			List<string> completeTweenSequenceKeys = new List<string>();
			List<TweenSequence> completeTweenSequences = new List<TweenSequence>();
			//更新tween
			foreach (var kvp in tweenSequences)
			{
				kvp.Value.UpdateTweenSequence(deltaTime);
				if (kvp.Value.complete)
				{
					completeTweenSequenceKeys.Add(kvp.Key);
				}
			}
			foreach (var tween in anonymousTweenSequences)
			{
				tween.UpdateTweenSequence(deltaTime);
				if (tween.complete)
				{
					completeTweenSequences.Add(tween);
				}
			}

			//处理已完成tween
			foreach (var key in completeTweenSequenceKeys)
			{
				var tween = tweenSequences[key];
				if (tween.loop)
				{
					tween.CallCompleteFunc();
					tween.Reset();
				}
				else
				{
					tweens.Remove(key);
					tween.CallCompleteFunc();
				}
			}
			foreach (var tween in completeTweenSequences)
			{
				if (tween.loop)
				{
					tween.CallCompleteFunc();
					tween.Reset();
				}
				else
				{
					anonymousTweenSequences.Remove(tween);
					tween.CallCompleteFunc();
				}
			}
		}
	}
}
